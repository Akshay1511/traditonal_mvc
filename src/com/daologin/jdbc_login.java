package com.daologin;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.beanlogin.Add_Login;
import com.util.CustomizedUtility;

public class jdbc_login {
	
	public void GetLogin(Add_Login lg) {
		Connection con=CustomizedUtility.getConnected();
		PreparedStatement prp=null;
		ResultSet rs=null;
		String query="select * from Customer_mvc where username=? and password=?";
		
		try {
			con.setAutoCommit(false);
			
			prp=con.prepareStatement(query);
			prp.setString(1,lg.getUsername());
			prp.setInt(2,lg.getPassword());
	
			rs=prp.executeQuery();
			con.commit();
			
			if(rs.next()) {
				System.out.println("CustId = "+rs.getInt(1));
				System.out.println("CustName = "+rs.getString(2));
				System.out.println("UserName = "+rs.getString(3));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
