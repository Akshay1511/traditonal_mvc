package com.fc;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/fc")
public class frontcontroller extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd;
		
		if(request.getParameter("type").equals("custReg")) {
		rd=request.getRequestDispatcher("/pages/cust_details.jsp");
		rd.forward(request, response);
		}
		else if (request.getParameter("type").equals("add_login")){
		   
			rd=request.getRequestDispatcher("/pages/login_details.jsp");
		    rd.forward(request, response);
				
			}
		}
	}


