package com.daoreg;

import java.sql.Connection;
import java.sql.PreparedStatement;
import com.beanreg.Add_Customer;
import com.util.CustomizedUtility;

public class Custdao {
	
	public void getCust( Add_Customer cust) {
		Connection con=CustomizedUtility.getConnected();
		String Query="insert into Customer_Mvc values(?,?,?,?)";
		PreparedStatement prp=null;
		
		try {
			prp=con.prepareStatement(Query);
			prp.setInt(1,cust.getCustId());
			prp.setString(2,cust.getCustName());
			prp.setString(3, cust.getUsername());
			prp.setInt(4,cust.getPassword());
			
			int r=prp.executeUpdate();
			 
			if(r>0) {
				
				System.out.println(r+" values are inserted");
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
