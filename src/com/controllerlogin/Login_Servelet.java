package com.controllerlogin;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.beanlogin.Add_Login;
import com.daologin.jdbc_login;

@WebServlet("/Login_Servelet")
public class Login_Servelet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		HttpSession session= request.getSession();
		
		Add_Login al=(Add_Login) session.getAttribute("log");
		
		jdbc_login jl=new jdbc_login();
		
		jl.GetLogin(al);
	  
		
	}

}
